# Theia

https://theia-ide.org/

## install

- コンテナ内でdockerを利用したいため、専用のイメージを作成
- Docker outside of Dockerをする場合、コンテナ内でコンテナ作成する際に指定する
  マウント時のホスト側ディレクトリは、Dockerデーモン側のホスト側パスになるため、
  コンテナ内で同じパスにマウントすることで混乱を避ける。
- Dockerクライアントはdockerイメージを使うのもありだが、ホストのクライアントをマウントすればサーバとバージョンを合わせるのが楽。

### 独自イメージ作成
```
cd docker
docker build -t mytheia:latest \
  --build-arg DOCKER_GROUP_ID=`cat /etc/group | grep docker | cut -d: -f3` \
  .
```

```
docker run \
  --name theia \
  -d \
  -p 18080:8080 \
  -v theia:/home/node \
  -v $(pwd):$(pwd) \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  mytheia \
    --hostname 0.0.0.0 --port 8080
```

### マウント＋永続化

```
docker run \
  --name theia \
  -it \
  -p 18080:8080 \
  -v theia:/home/node \
  -v $(pwd):$(pwd) \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  -v /usr/bin/docker:/usr/bin/docker:ro \
  -v /usr/local/bin/docker-compose:/usr/local/bin/docker-compose:ro \
  -e DOCKER_GROUP_ID=`cat /etc/group | grep docker | cut -d: -f3` \
  node:10 \
    bash
```

```
groupadd -g ${DOCKER_GROUP_ID} docker && usermod -aG ${DOCKER_GROUP_ID} node

su node
cd /home/node

tee package.json <<"EOF" >/dev/null
{
  "private": true,
  "dependencies": {
    "@theia/callhierarchy": "next",
    "@theia/file-search": "next",
    "@theia/git": "next",
    "@theia/json": "next",
    "@theia/markers": "next",
    "@theia/messages": "next",
    "@theia/mini-browser": "next",
    "@theia/navigator": "next",
    "@theia/outline-view": "next",
    "@theia/plugin-ext-vscode": "next",
    "@theia/preferences": "next",
    "@theia/preview": "next",
    "@theia/search-in-workspace": "next",
    "@theia/terminal": "next"
  },
  "devDependencies": {
    "@theia/cli": "next"
  },
  "scripts": {
    "prepare": "yarn run clean && yarn build && yarn run download:plugins",
    "clean": "theia clean",
    "build": "theia build --mode development",
    "start": "theia start --plugins=local-dir:plugins",
    "download:plugins": "theia download:plugins"
  },
  "theiaPluginsDir": "plugins",
  "theiaPlugins": {
    "vscode-builtin-css": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/css-1.39.1-prel.vsix",
    "vscode-builtin-html": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/html-1.39.1-prel.vsix",
    "vscode-builtin-javascript": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/javascript-1.39.1-prel.vsix",
    "vscode-builtin-json": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/json-1.39.1-prel.vsix",
    "vscode-builtin-markdown": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/markdown-1.39.1-prel.vsix",
    "vscode-builtin-npm": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/npm-1.39.1-prel.vsix",
    "vscode-builtin-scss": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/scss-1.39.1-prel.vsix",
    "vscode-builtin-typescript": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/typescript-1.39.1-prel.vsix",
    "vscode-builtin-typescript-language-features": "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/typescript-language-features-1.39.1-prel.vsix"
  }
}
EOF

### インストール。初回のみ
yarn

### 起動
yarn start --hostname 0.0.0.0 --port 8080
```


## plugin

`plugins` 直下に `.vsix` ファイルを格納すると有効になる。

## backup / restore(docker volume)

```
docker run \
  --rm \
  -v theia:/home/node \
  -v `pwd`:/backup \
  busybox \
    tar cvf /backup/backup.tar /home/node
```

```
docker run \
  --rm \
  -v theia:/home/node \
  -v `pwd`:/backup \
  busybox \
    tar xvf /backup/backup.tar
```

